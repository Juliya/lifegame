﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace LifeGame
{
    public partial class LifeGame : Form
    {
        public static short SizeAreaX = 40;
        public static short SizeAreaY = 40;

        private readonly Point[,] _area = new Point[SizeAreaX, SizeAreaY];

        public LifeGame()
        {
            InitializeComponent();
            InitAreaView();
            FeelZeroGeneration();
            ShowCurrentGeneration();

        }

        private void FeelZeroGeneration()
        {
            var random = new Random();

            for (int i = 0; i < SizeAreaX; i++)
            {
                for (int j = 0; j < SizeAreaY; j++)
                {
                    Point point = new Point();

                    bool isAlive = random.Next() % 2 == 0;

                    point.IsAlive = isAlive;
                    point.IsAliveBefore = isAlive;
                    _area[i, j] = point;
                }
            }


        }

        public int AliveNear(int x, int y)
        {
            int countAlive = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    try
                    {
                        if (!(i == 0 && j == 0) && _area[x + i, y + j].IsAliveBefore)
                            countAlive++;
                    }
                    catch (Exception e)
                    {
                    }
                }
            }

            return countAlive;
        }

        public void NextGeneration()
        {
            for (int i = 0; i < SizeAreaX; i++)
            {
                for (int j = 0; j < SizeAreaY; j++)
                {
                    if ((_area[i, j].IsAlive && AliveNear(i, j) == 2) || (AliveNear(i, j) == 3))
                    {
                        _area[i, j].IsAliveBefore = true;
                    }
                    else
                    {
                        _area[i, j].IsAliveBefore = false;
                    }
                }
            }
        }

        public void InitAreaView()
        {
            for (int i = 0; i < SizeAreaY; i++)
            {
                areaView.Columns.Add((i + 1).ToString(), (i + 1).ToString());
            }

            areaView.Rows.Add(SizeAreaX);

        }

        public void ShowCurrentGeneration()
        {
            for (int i = 0; i < SizeAreaX; i++)
            {
                for (int j = 0; j < SizeAreaY; j++)
                {
                    if (_area[i, j].IsAlive)
                    {
                        areaView[i, j].Style.BackColor = Color.DarkGreen;
                        areaView[i, j].Value = "ЖЫВ";
                    }
                    else
                    {
                        areaView[i, j].Style.BackColor = Color.Firebrick;
                        areaView[i, j].Value = "Нет";

                    }
                }
            }
        }

        public void FlashGeneration()
        {
            for (int i = 0; i < SizeAreaX; i++)
            {
                for (int j = 0; j < SizeAreaY; j++)
                {
                    _area[i, j].IsAlive = _area[i, j].IsAliveBefore;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NextGeneration();
            FlashGeneration();
            ShowCurrentGeneration();
        }
    }
}
