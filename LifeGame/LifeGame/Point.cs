﻿namespace LifeGame
{
    class Point
    {
        public bool IsAliveBefore { get; set; }
        public bool IsAlive { get; set; }
    }
}